## Altapay Technical Test
### About 
 All the TODOs has been completed for the service along with the test cases. Basically Service 
 consists of two flows only one is to capture payment and second is to release 
 the payments. 

## Technology Stack

Language : JAVA 1.8.

Framework : Spring-boot: 2.4.2 

Build tool : maven


## Build the Service

Use these steps to run the service.

- Clone the repository.

- Redirect to Project's main folder. Enter following command to build the project.
  
           mvn clean install -U
           
        
       
- To run the application. Enter Following command.

           mvn spring-boot:run
  
____  

For any concern Kindly contact to following emailId.

       leo4amit@gmail.com