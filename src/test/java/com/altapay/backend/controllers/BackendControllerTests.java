package com.altapay.backend.controllers;

import com.altapay.backend.exceptions.CapturePaymentException;
import com.altapay.backend.exceptions.InventoryNotAvailableException;
import com.altapay.backend.model.IModelFactory;
import com.altapay.backend.model.OrderLine;
import com.altapay.backend.model.Product;
import com.altapay.backend.model.ShopOrder;
import com.altapay.backend.repositories.ShopOrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BackendController.class})
public class BackendControllerTests {

    private static final String ORDER_ID = "OCPA2021030945";

    @MockBean
    private ShopOrderRepository shopOrderRepository;

    @MockBean
    IModelFactory modelFactory;

    @Mock
    private ShopOrder shopOrder;

    @Autowired
    private BackendController controller;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        when(shopOrderRepository.loadShopOrder(ORDER_ID)).thenReturn(shopOrder);

    }

    @Test
    public void captureReservationGetsTheOrderFromTheRepository() {
        controller.capturePayment(ORDER_ID);
        verify(shopOrderRepository).loadShopOrder(ORDER_ID);
    }

    @Test
    public void captureReservationMustInvokeCaptureOnTheOrder() {
        controller.capturePayment(ORDER_ID);

        try {
            verify(shopOrder).capture();
        } catch (CapturePaymentException e) {
            e.printStackTrace();
        } catch (InventoryNotAvailableException e) {
            e.printStackTrace();
        }
    }
}
