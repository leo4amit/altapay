package com.altapay.backend.model;

import com.altapay.backend.controllers.BackendController;
import com.altapay.backend.ioc.BackendContainer;
import com.altapay.backend.repositories.InventoryRepository;
import com.altapay.backend.repositories.ShopOrderRepository;
import com.altapay.backend.services.InventoryService;
import com.altapay.backend.services.MerchantApiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;


@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BackendContainer.class, BackendController.class, ShopOrder.class, InventoryService.class, InventoryRepository.class, MerchantApiService.class})
public class ShopOrderTest {

    private static final String ID = "orderid";
    private static final String PAYMENTID = "paymentid";


    @Autowired
    IModelFactory modelFactory;

    private ShopOrder order;

    @Autowired
    BackendController backendController;

    @MockBean
    private ShopOrderRepository shopOrderRepository;

    @MockBean
    InventoryRepository inventoryRepository;

    @BeforeEach
    public void setUp() throws Exception {
        order = modelFactory.getShopOrder();
        order.setId(ID);
        order.setPaymentId(PAYMENTID);
        List<OrderLine> orderLines = new ArrayList<>();
        orderLines.add(getOrderLine("1", "Keyboard", 1));
        order.setOrderLines(orderLines);

        when(shopOrderRepository.loadShopOrder(ID)).thenReturn(order);

        Inventory inventory = new Inventory();

        Product product = new Product();
        product.setId("1");
        product.setName("Keyboard");
        inventory.setInventory(5);

        inventory.setProduct(product);

        when(inventoryRepository.load("1")).thenReturn(inventory);
    }


    private OrderLine getOrderLine(String productId, String name, int quantity) {
        OrderLine orderLine = modelFactory.getOrderLine();
        Product product = modelFactory.getProduct();
        product.setId(productId);
        product.setName(name);
        orderLine.setProduct(product);
        orderLine.setQuantity(quantity);
        return orderLine;
    }

    @Test
    public void executeCapture_inventoryIsChecked() {
        // Implement test
        for (OrderLine orderLine : order.getOrderLines()) {
            assertTrue(order.getInventoryService().checkInventory(orderLine.getProduct(), orderLine.getQuantity()));
        }
    }

    @Test
    public void executeCapture_paymentIsCapturedThroughApiService() {
        // Implement test
        //fail("Not yet implemented");
        assertTrue(backendController.capturePayment(ID));
    }

    @Test
    public void executeRelease_paymentIsReleasedThroughApiService() {
        // Implement test
        //fail("Not yet implemented");
        assertTrue(backendController.releasePayment(ID));
    }

}
