package com.altapay.backend.exceptions;

public class InventoryNotAvailableException extends Exception {

    public InventoryNotAvailableException(String id) {
        super("Inventory Not Available For Product :" + id);
    }
}
