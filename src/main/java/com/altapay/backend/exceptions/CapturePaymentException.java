package com.altapay.backend.exceptions;

public class CapturePaymentException extends Exception {

    private static final long serialVersionUID = -4005748541319236349L;

    public CapturePaymentException(String id) {
        super("Unable to capture payment for shopOrderId:" + id);
    }
}
