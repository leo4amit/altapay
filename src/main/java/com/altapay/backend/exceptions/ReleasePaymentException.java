package com.altapay.backend.exceptions;

public class ReleasePaymentException extends Exception {

    private static final long serialVersionUID = -4005748541319236349L;

    public ReleasePaymentException(String id) {
        super("Unable to release payment for shopOrderId:" + id);
    }
}
