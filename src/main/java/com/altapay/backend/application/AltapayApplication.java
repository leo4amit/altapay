package com.altapay.backend.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.altapay.*")
public class AltapayApplication {

    public static void main(String[] args) {
        System.out.println("application is running");
        SpringApplication.run(AltapayApplication.class, args);
    }

}
