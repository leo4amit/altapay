package com.altapay.backend.model;

import com.altapay.backend.exceptions.CapturePaymentException;
import com.altapay.backend.exceptions.InventoryNotAvailableException;
import com.altapay.backend.exceptions.MerchantApiServiceException;
import com.altapay.backend.exceptions.ReleasePaymentException;
import com.altapay.backend.services.CaptureResponse;
import com.altapay.backend.services.InventoryService;
import com.altapay.backend.services.MerchantApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class ShopOrder {

    private static final Logger logger = LoggerFactory.getLogger(ShopOrder.class);

    String id;
    String paymentId;
    List<OrderLine> orderLines;

    private InventoryService inventoryService;
    private MerchantApiService merchantApiService;

    public InventoryService getInventoryService() {
        return inventoryService;
    }

    public MerchantApiService getMerchantApiService() {
        return merchantApiService;
    }

    public ShopOrder(InventoryService inventoryService, MerchantApiService merchantApiService) {
        this.inventoryService = inventoryService;
        this.merchantApiService = merchantApiService;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public String getId() {
        return id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void capture() throws CapturePaymentException, InventoryNotAvailableException {
        // 1: use the InventoryService to check inventory before capturing
        if (orderLines != null) {
            for (OrderLine orderLine : orderLines) {
                if (orderLine.getProduct() != null) {
                    boolean isInventoryAvailable = inventoryService.checkInventory(orderLine.getProduct(), orderLine.getQuantity());

                    if (!isInventoryAvailable) {
                        throw new InventoryNotAvailableException(orderLine.getProduct().getId());
                    }
                }
            }
        }

        //: Use the MerchantApiService to capture the payment.

        CaptureResponse captureResponse = null;
        try {
            captureResponse = merchantApiService.capturePayment(this);
        } catch (MerchantApiServiceException e) {
            logger.error("MerchantApiServiceException while capturing the payment ", e);
            throw new CapturePaymentException(id);
        }


        //  use the InventoryService to take from inventory after capturing
        for (OrderLine orderLine : orderLines) {
            if (orderLine.getProduct() != null) {
                boolean isInventoryAvailable = inventoryService.takeFromInventory(orderLine.getProduct(), orderLine.getQuantity());

            }
        }

    }

    // Release is a synonym for canceling a payment
    public void release() throws ReleasePaymentException {
        try {
            // Use the MerchantApiService to release the payment.
            merchantApiService.releasePayment(this);
            //set paymentId;
        } catch (MerchantApiServiceException e) {
            logger.error("MerchantApiServiceException while releasing the payments ", e);
            throw new ReleasePaymentException(id);
        }
    }
}
