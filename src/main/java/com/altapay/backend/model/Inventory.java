package com.altapay.backend.model;

import java.util.StringJoiner;

public class Inventory
{
	private Product product;
	private int inventory;
	
	public Product getProduct() 
	{
		return product;
	}
	
	public void setProduct(Product product) 
	{
		this.product = product;
	}
	
	public int getInventory() 
	{
		return inventory;
	}
	
	public void setInventory(int inventory) 
	{
		this.inventory = inventory;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Inventory.class.getSimpleName() + "[", "]")
				.add("product=" + product)
				.add("inventory=" + inventory)
				.toString();
	}
}
