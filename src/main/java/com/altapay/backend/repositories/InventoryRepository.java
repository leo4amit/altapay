package com.altapay.backend.repositories;

import com.altapay.backend.model.Inventory;
import com.altapay.backend.model.Product;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryRepository {
    public Inventory load(String productId) {
        // We dont need to implement this, write the rest of the code as if this has been implemented

        //this is implemented just to run api's although this can be mocked using mockito
        Inventory inventory = new Inventory();

        Product product = new Product();
        product.setId(productId);
        product.setName("Keyboard");
        inventory.setInventory(5);

        inventory.setProduct(product);
        return inventory;
    }

    public void save(Inventory inventory)
            throws Exception {
        // We dont need to implement this, write the rest of the code as if this has been implemented
    }
}
