package com.altapay.backend.controllers;

import com.altapay.backend.exceptions.CapturePaymentException;
import com.altapay.backend.exceptions.InventoryNotAvailableException;
import com.altapay.backend.exceptions.ReleasePaymentException;
import com.altapay.backend.model.ShopOrder;
import com.altapay.backend.repositories.ShopOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/altapay/payment/")
public class BackendController {

    private static final Logger logger = LoggerFactory.getLogger(BackendController.class);

    @Autowired
    private ShopOrderRepository shopOrderRepository;

    @RequestMapping(value = "capture/{shopOrderId}", method = RequestMethod.PUT)
    public Boolean capturePayment(@PathVariable String shopOrderId) {
        ShopOrder order = shopOrderRepository.loadShopOrder(shopOrderId);

        try {
            order.capture();
            logger.info("order " + order);
            // Save the model after capturing
            shopOrderRepository.saveShopOrder(order);
            return true;
        } catch (CapturePaymentException e) {
            logger.info("CapturePaymentException :" + e.getMessage());
        } catch (InventoryNotAvailableException e) {
            logger.info("InventoryNotAvailableException :" + e.getMessage());
        }
        return false;

    }

    @RequestMapping(value = "release/{shopOrderId}")
    public Boolean releasePayment(@PathVariable String shopOrderId) {
        // load the shop order
        ShopOrder order = shopOrderRepository.loadShopOrder(shopOrderId);
        try {
            logger.info("order " + order);
            order.release();
            //  Save the model after releasing
            shopOrderRepository.saveShopOrder(order);
            return true;
        } catch (ReleasePaymentException e) {
            logger.info("ReleasePaymentException :" + e.getMessage());
        }
        return false;
    }

    @GetMapping(value = "health")
    public String getHealth() {
        return "Health is fine";
    }

}
