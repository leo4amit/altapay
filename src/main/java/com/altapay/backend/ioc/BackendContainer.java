package com.altapay.backend.ioc;

import com.altapay.backend.model.*;
import com.altapay.backend.repositories.ShopOrderRepository;
import com.altapay.backend.services.InventoryService;
import com.altapay.backend.services.MerchantApiService;
import com.altapay.util.HttpUtil;
import com.altapay.util.XpathUtil;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
public class BackendContainer implements IModelFactory {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public ShopOrderRepository getShopOrderRepository() {
        return new ShopOrderRepository(this);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public InventoryService getInventoryService() {
        return new InventoryService();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public MerchantApiService getMerchantApiService() {
        return new MerchantApiService(getHttpUtil(), getXPathUtil());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public HttpUtil getHttpUtil() {
        return new HttpUtil();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public XpathUtil getXPathUtil() {
        return new XpathUtil();
    }


    @RequestScope
    @Override
    public ShopOrder getShopOrder() {
        // initialize a new ShopOrder with it's dependencies
        return new ShopOrder(getInventoryService(), getMerchantApiService());
    }

    @Override
    public Inventory getInventory() {
        //  initialize a new Inventory with it's dependencies
        return new Inventory();
    }

    @Override
    public OrderLine getOrderLine() {
        //  initialize a new OrderLine with it's dependencies
        return new OrderLine();
    }

    @Override
    public Product getProduct() {
        //  initialize a new Product with it's dependencies
        return new Product();
    }

}
