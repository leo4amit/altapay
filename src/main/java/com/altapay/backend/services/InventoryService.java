package com.altapay.backend.services;

import com.altapay.backend.model.Inventory;
import com.altapay.backend.model.Product;
import com.altapay.backend.repositories.InventoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryService {

    private static final Logger logger = LoggerFactory.getLogger(InventoryService.class);

    @Autowired
    private InventoryRepository repository;

    public boolean checkInventory(Product product, int quantity) {
        //  implement check inventory, as you see fit
        Inventory loadedInventory = repository.load(product.getId());
        logger.info("requested product hash : " + product.hashCode() + ", loaded product hash : " + loadedInventory.getProduct().hashCode());

        logger.info("loadedInventory : " + loadedInventory);
        if (loadedInventory != null && loadedInventory.getProduct().equals(product)
                && quantity <= loadedInventory.getInventory()) {
            return true;
        }
        return false;
    }

    //would be transactional
    public boolean takeFromInventory(Product product, int quantity) {
        // implement take from inventory, as you see fit
        try {
            Inventory loadedInventory = repository.load(product.getId());
            int inventoryQuantity = loadedInventory.getInventory() - quantity;
            loadedInventory.setInventory(inventoryQuantity);
            repository.save(loadedInventory);
            return true;
        } catch (Exception e) {
            logger.error("exception while taking from inventory :", e);
            return false;
        }
    }
}
